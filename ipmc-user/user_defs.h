/*
Description:

If you want to add custom header file information to your project (e.g. to use your own symbolic names for
macros in config.xml) you can add them to this file.


Example:
Add the following line to this file:
#define POL_DCDC_ENABLE USER_IO_3_ACTL


Subsequently you can use the macro e.g. in the power-up sequence in your config.xml like that:
 
    <PowerONSeq>
      <step>PSQ_ENABLE_SIGNAL(CFG_PAYLOAD_DCDC_EN_SIGNAL)</step>           <!-- Enable the 12V DC/DC converter -->
      <step>PSQ_ENABLE_SIGNAL(POL_DCDC_ENABLE)</step>                      <!-- Enable the POL DC/DC converters -->
    </PowerONSeq>
 
Note: 
1.	This in an artificial example. Do not use it as is. 
2.	Another case where the defines can be useful is when declaring GPIO sensors, see the example below (used in config.xml):
#define PG_PAYLOAD      USER_IO_9_ACTH

*/
