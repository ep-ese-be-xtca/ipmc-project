/***********************************************************************

Nom ......... : user_oem.c
Role ........ : Example of OEM command definition
Auteur ...... : Julian Mendez <julian.mendez@cern.ch>
Version ..... : V1.0 - 25/10/2017

Compilation :
	- Add the file into the FILES list located in ipmc-user/nr-mkfrag

***********************************************************************/
#include <defs.h>
#include <cfgint.h>

#include <cmd.h>
#include <appdefs.h>
#include <app/signal.h>
#include <log.h>
#include <debug.h>
#include <i2c_dev.h>

#define OEM_IANA_CODE    		0x000060 /* vendor id (IANA code)*/
const unsigned char cmd_prefix[] = {OEM_IANA_CODE};

#define OEM_USER_I2CEXAMPLE  	0x44 	/* command code */

/* 
 * Name: oem_user_i2cexample
 *
 * Parameters:
 *		- header: IPMI command header
 *		- rep: reply data buffer
 *		- compcode: returned completion code
 *
 * Description: Called when the related OEM command is received. This command send data to the I2C bus
 *			    and returns the value read back.
 */
static cmd_ret_t oem_user_i2cexample(msg_header_t *header,  unsigned char *rep, unsigned char *compcode)
{
	unsigned char i2crep;

	unsigned short addrptr, regptr;
	unsigned char val;

	/* Get OEM command parameter: I2C address and value to be written */
	addrptr = main_ring_get();							//main_ring_get(): Get first parameter (Device addr)
	regptr = ((unsigned short)main_ring_get())	<< 8;	//main_ring_get(): Get following parameter (Register pointer [MSB])
	regptr |= main_ring_get();							//main_ring_get(): Get following parameter (Register pointer [LSB])
	val = main_ring_get();								//main_ring_get(): Get following parameter (Value to be written)
	
	//main_ring_get(): can be called as many time as there are parameter available

	/* 
	
	A prefix must be added to the I2C address to specify which bus is used to send a command:
		addrptr |= 0x000  => Sensor I2C
		addrptr |= 0x100  => Mgt I2C
		
	*/
	addrptr |= 0x100;
	
	/* Print in debug console */
	debug_printf("<I> Write %02xh @%02xh[reg=%04xh] \n\r", val, addrptr, regptr);
	
	/* 
	Write command (2byte registers address):
		- addrptr: device I2C address
		- regptr: 16bit register ptr
		- val: data byte array
		- 1: number of byte to be written
		
	For 1 byte register address, equivalent function exists:
		i2c_dev_write_reg(addrptr, regptr, &val, 1) (where regptr is an unsigned char)
	*/
	i2c_dev_write_2bytesReg(addrptr, regptr, &val, 1);
	
	/* 
	Read command (2byte registers address):
		- addrptr: device I2C address
		- regptr: 16bit register ptr
		- i2crep: data byte array (read value)
		- 1: number of byte to be read
		
	For 1 byte register address, equivalent function exists:
		i2c_dev_read_reg(addrptr, regptr, &val, 1) (where regptr is an unsigned char)
	*/
	i2c_dev_read_2bytesReg(addrptr, regptr, &i2crep, 1);
	
	/* Print in debug console */
	debug_printf("<_> Read %02xh @%02xh[reg=%04xh] \n", i2crep, addrptr, regptr);

	/* Add the i2c byte read in the IPMI reply */
	rep[4] = i2crep;

    return 0;
}

/*
 * MASTER_CMD_HANDLER: Register an IPMI command handler 
 *
 * Parameters:
 * 		- IPMI_NETFN_OEM_GROUP: netFunction (OEM command)
 *		- OEM_USER_I2CEXAMPLE: Command
 *		- cmd_prefix: IANA code (used to check the OEM command, specified by the standard)
 *		- 3: Prefix size, 3 bytes
 *		- CMD_FL_NONE: Mask of command handling flag
 *		- 5: Request data length (including prefix length)
 *		- 5: Response length (including completion code and prefix length)
 *		- oem_user_i2cexample: Pointer to command handling function
 */
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_USER_I2CEXAMPLE, cmd_prefix, 3, CMD_FL_NONE, 7, 5, oem_user_i2cexample);
