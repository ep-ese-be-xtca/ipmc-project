/***********************************************************************

Name......... : user_oem_examples.c
Role ........ : Examples of OEM functions
Auteur ...... : Markus Joos <markus.joos@cern.ch> based on code of Julian Mendez <julian.mendez@cern.ch>
Version ..... : V1.0 - 9. Mar. 2022

Compilation :
	- Add the file into the FILES list located in ipmc-user/nr-mkfrag

***********************************************************************/
#error "ipmc-user/user_oem_examples.c: This file provides examples for user OEM commands. You cannot use this file at the same time as ipmc-user/user_oem.c. We recommand that you copy those OEM commands from this file that you want to use (if any) to user_oem.c and then delete ipmc-user/user_oem_examples.c to unblock the compilation"

#include <defs.h>
#include <cfgint.h>

#include <cmd.h>
#include <appdefs.h>
#include <app/signal.h>
#include <log.h>
#include <debug.h>
#include <cfgint.h>
#include <i2c_dev.h>

#include <fru_led.h>
#include <app/master/mainfru.h>
#include <iface_lan.h>

#define OEM_IANA_CODE    0x000060 /* vendor id */
const unsigned char cmd_prefix[] = { OEM_IANA_CODE };

#define OEM_SET_USERIO    	0x44 /* command code */
#define OEM_SET_IPMIO     	0x45 
#define OEM_CLR_USERIO    	0x46 
#define OEM_CLR_IPMIO     	0x47 
#define OEM_GET_USERIO    	0x48 
#define OEM_GET_IPMIO     	0x49 
#define OEM_GET_MACADDR         0x4f
#define OEM_TEST_MGT_I2C  	0x50
#define OEM_TEST_SENSOR_I2C     0x51
#define OEM_SET_MACADDR         0x52
#define OEM_TEST_GETLM75        0x53
#define OEM_SET_IPMCSN          0x54
#define OEM_GET_IPMCSN          0x55
#define OEM_SET_IPMCBN          0x56
#define OEM_GET_IPMCBN          0x57

static cmd_ret_t oem_set_macaddr(msg_header_t *header, unsigned char *rep, unsigned char *compcode){

	unsigned char mac_addr[6];
	int i;

        //debug_printf("you are in oem_set_macaddr. \n\r");

	for(i=0; i < 6; i++){
		mac_addr[5-i] = main_ring_get();
	}
	i2c_eeprom_write(CFG_MACADDR_INFO_ADDR, CFG_MACADDR_INFO_OFFSET, mac_addr, 6);
	//memcpy(lan_channel_config[0].macaddr, mac_addr, 6);

	return 0;
}

static cmd_ret_t oem_get_macaddr(msg_header_t *header, unsigned char *rep, unsigned char *compcode){

	unsigned char mac_addr[6];
	int i;

        //debug_printf("you are in oem_get_macaddr. \n\r");

	i2c_eeprom_read(CFG_MACADDR_INFO_ADDR, CFG_MACADDR_INFO_OFFSET, mac_addr, 6);
	debug_printf("MAC = [0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x] \n", mac_addr[5], mac_addr[4], mac_addr[3], mac_addr[2], mac_addr[1], mac_addr[0]);

	*compcode = IPMI_SUCCESS;

	for(i = 0; i < 6; i++){
		rep[4 + i] = mac_addr[5 - i];
	}

	return 0;	
}

static cmd_ret_t oem_set_sn(msg_header_t *header, unsigned char *rep, unsigned char *compcode){

	unsigned char sn[9];
	int i;

        //debug_printf("you are in oem_set_sn. \n\r");
        //debug_printf("CFG_IPMCSN_INFO_ADDR   = %d \n\r", CFG_IPMCSN_INFO_ADDR);
        //debug_printf("CFG_IPMCSN_INFO_OFFSET = %d \n\r", CFG_IPMCSN_INFO_OFFSET);

	for(i=0; i < 9; i++){
		sn[8-i] = main_ring_get();
                //debug_printf("Serial number byte %d = %d\n\r", i , sn[8-i]);

	}
	i2c_eeprom_write(CFG_IPMCSN_INFO_ADDR, CFG_IPMCSN_INFO_OFFSET, sn, 9);
	return 0;
}


static cmd_ret_t oem_get_sn(msg_header_t *header, unsigned char *rep, unsigned char *compcode){

	unsigned char sn[9];
	int i;

        //debug_printf("you are in oem_get_sn. \n\r");
	i2c_eeprom_read(CFG_IPMCSN_INFO_ADDR, CFG_IPMCSN_INFO_OFFSET, sn, 9);
	debug_printf("Read IPMC S/N [%d %d %d %d %d %d %d %d %d] \n", sn[8], sn[7], sn[6], sn[5], sn[4], sn[3], sn[2], sn[1], sn[0]);

	*compcode = IPMI_SUCCESS;

	for(i=0; i < 9; i++){
		rep[4+i] = sn[8-i];
	}

	return 0;
}

static cmd_ret_t oem_set_bn(msg_header_t *header, unsigned char *rep, unsigned char *compcode){

	unsigned char bn[9];
	int i;

        //debug_printf("you are in oem_set_bn. \n\r");
	for(i=0; i < 9; i++){
		bn[8-i] = main_ring_get();
	}
	i2c_eeprom_write(CFG_IPMCBN_INFO_ADDR, CFG_IPMCBN_INFO_OFFSET, bn, 9);
	return 0;
}


static cmd_ret_t oem_get_bn(msg_header_t *header, unsigned char *rep, unsigned char *compcode){

	unsigned char bn[9];
	int i;

        //debug_printf("you are in oem_get_bn. \n\r");
	i2c_eeprom_read(CFG_IPMCBN_INFO_ADDR, CFG_IPMCBN_INFO_OFFSET, bn, 9);
	debug_printf("Read IPMC B/N [%c%c%c%c%c%c%c%c%c] \n",bn[8],bn[7],bn[6],bn[5],bn[4],bn[3],bn[2],bn[1],bn[0]);

	*compcode = IPMI_SUCCESS;

	for(i=0; i < 9; i++){
		rep[4+i] = bn[8-i];
	}

	return 0;
}

static cmd_ret_t oem_set_userio(msg_header_t *header, unsigned char *rep, unsigned char *compcode) {
	unsigned char io_id;

	signal_t userio_sig[] = { USER_IO_0, USER_IO_1, USER_IO_2, USER_IO_3,
			USER_IO_4, USER_IO_5, USER_IO_6, USER_IO_7, USER_IO_8, USER_IO_9,
			USER_IO_10, USER_IO_11, USER_IO_12, USER_IO_13, USER_IO_14,
			USER_IO_15, USER_IO_16, USER_IO_17, USER_IO_18, USER_IO_19,
			USER_IO_20, USER_IO_21, USER_IO_22, USER_IO_23, USER_IO_24,
			USER_IO_25, USER_IO_26, USER_IO_27, USER_IO_28, USER_IO_29,
			USER_IO_30, USER_IO_31, USER_IO_32, USER_IO_33, USER_IO_34 };

	*compcode = IPMI_SUCCESS;

	io_id = main_ring_get();

	if (io_id > 34)
		*compcode = IPMI_PARAMETER_OUT_OF_RANGE;
	else
		signal_activate(&userio_sig[io_id]);

	return 0;
}

static cmd_ret_t oem_set_ipmio(msg_header_t *header, unsigned char *rep, unsigned char *compcode) {
	unsigned char io_id;

	signal_t ipmio_sig[] = { IPM_IO_0, IPM_IO_1, IPM_IO_2, IPM_IO_3, IPM_IO_4,
			IPM_IO_5, IPM_IO_6, IPM_IO_7, IPM_IO_8, IPM_IO_9, IPM_IO_10,
			IPM_IO_11, IPM_IO_12, IPM_IO_13, IPM_IO_14, IPM_IO_15 };

	*compcode = IPMI_SUCCESS;

	io_id = main_ring_get();

	if (io_id > 15)
		*compcode = IPMI_PARAMETER_OUT_OF_RANGE;
	else
		signal_activate(&ipmio_sig[io_id]);

	return 0;
}

static cmd_ret_t oem_clr_userio(msg_header_t *header, unsigned char *rep, unsigned char *compcode) {
	unsigned char io_id;

	signal_t userio_sig[] = { USER_IO_0, USER_IO_1, USER_IO_2, USER_IO_3,
			USER_IO_4, USER_IO_5, USER_IO_6, USER_IO_7, USER_IO_8, USER_IO_9,
			USER_IO_10, USER_IO_11, USER_IO_12, USER_IO_13, USER_IO_14,
			USER_IO_15, USER_IO_16, USER_IO_17, USER_IO_18, USER_IO_19,
			USER_IO_20, USER_IO_21, USER_IO_22, USER_IO_23, USER_IO_24,
			USER_IO_25, USER_IO_26, USER_IO_27, USER_IO_28, USER_IO_29,
			USER_IO_30, USER_IO_31, USER_IO_32, USER_IO_33, USER_IO_34 };

	*compcode = IPMI_SUCCESS;

	io_id = main_ring_get();

	if (io_id > 34)
		*compcode = IPMI_PARAMETER_OUT_OF_RANGE;
	else
		signal_deactivate(&userio_sig[io_id]);

	return 0;
}

static cmd_ret_t oem_clr_ipmio(msg_header_t *header, unsigned char *rep, unsigned char *compcode) {
	unsigned char io_id;

	signal_t ipmio_sig[] = { IPM_IO_0, IPM_IO_1, IPM_IO_2, IPM_IO_3, IPM_IO_4,
			IPM_IO_5, IPM_IO_6, IPM_IO_7, IPM_IO_8, IPM_IO_9, IPM_IO_10,
			IPM_IO_11, IPM_IO_12, IPM_IO_13, IPM_IO_14, IPM_IO_15 };

	*compcode = IPMI_SUCCESS;

	io_id = main_ring_get();

	if (io_id > 15)
		*compcode = IPMI_PARAMETER_OUT_OF_RANGE;
	else
		signal_deactivate(&ipmio_sig[io_id]);

	return 0;
}

static cmd_ret_t oem_get_userio(msg_header_t *header, unsigned char *rep, unsigned char *compcode) {
	unsigned char io_id;

	signal_t userio_sig[] = { USER_IO_0, USER_IO_1, USER_IO_2, USER_IO_3,
			USER_IO_4, USER_IO_5, USER_IO_6, USER_IO_7, USER_IO_8, USER_IO_9,
			USER_IO_10, USER_IO_11, USER_IO_12, USER_IO_13, USER_IO_14,
			USER_IO_15, USER_IO_16, USER_IO_17, USER_IO_18, USER_IO_19,
			USER_IO_20, USER_IO_21, USER_IO_22, USER_IO_23, USER_IO_24,
			USER_IO_25, USER_IO_26, USER_IO_27, USER_IO_28, USER_IO_29,
			USER_IO_30, USER_IO_31, USER_IO_32, USER_IO_33, USER_IO_34 };

	*compcode = IPMI_SUCCESS;

	io_id = main_ring_get();

	rep[0] = 0x00;

	if (io_id > 34)
		*compcode = IPMI_PARAMETER_OUT_OF_RANGE;
	else {
		signal_set_pin(&userio_sig[io_id], SIGNAL_HIGHZ);
		debug_printf("Pin (%d): 0x%02x", io_id, signal_read(&userio_sig[io_id]));
		rep[4] = signal_read(&userio_sig[io_id]);
	}

	return 0;
}

static cmd_ret_t oem_get_ipmio(msg_header_t *header, unsigned char *rep, unsigned char *compcode) {
	unsigned char io_id;

	signal_t ipmio_sig[] = { IPM_IO_0, IPM_IO_1, IPM_IO_2, IPM_IO_3, IPM_IO_4,
			IPM_IO_5, IPM_IO_6, IPM_IO_7, IPM_IO_8, IPM_IO_9, IPM_IO_10,
			IPM_IO_11, IPM_IO_12, IPM_IO_13, IPM_IO_14, IPM_IO_15 };

	*compcode = IPMI_SUCCESS;

	io_id = main_ring_get();

	rep[0] = 0x00;

	if (io_id > 15)
		*compcode = IPMI_PARAMETER_OUT_OF_RANGE;
	else {
		signal_set_pin(&ipmio_sig[io_id], SIGNAL_HIGHZ);
		rep[4] = signal_read(&ipmio_sig[io_id]);
	}
	return 0;
}

static cmd_ret_t oem_test_mgti2c(msg_header_t *header, unsigned char *rep,
		unsigned char *compcode) {
	unsigned char i2crep;

	unsigned short addrptr;
	unsigned char val;

	addrptr = main_ring_get();
	addrptr <<= 8;
	addrptr |= main_ring_get();

	val = main_ring_get();

	debug_printf("Test MGT interface (write %02xh @%04xh [%d]) -> ", val, addrptr, i2c_dev_write_2bytesReg(0x1ae, addrptr, &val, 1));
	i2c_dev_read_2bytesReg(0x1ae, addrptr, &i2crep, 1);
	debug_printf("Check value (%02xh) \n", i2crep);

	rep[4] = i2crep;

	return 0;
}

static cmd_ret_t oem_test_sensori2c(msg_header_t *header, unsigned char *rep,
		unsigned char *compcode) {
	int ret;
	unsigned char addr = main_ring_get();

	unsigned short ch_addr = 0x0200 | addr;

	debug_printf("Test Sensor interface (%02xh address) \n", ch_addr);

	ret = i2c_io_ext(i2c_masteronly_channel_from_address(ch_addr), ch_addr
			| I2C_START | I2C_STOP, NULL, 0);
	if ((I2C_OK != i2c_get_last_error(i2c_masteronly_channel_from_address(
			ch_addr))) || (0 != ret)) {
		debug_printf("\t-> Error \n");
		rep[4] = 0xFF;
	} else {
		debug_printf("\t-> Done \n");
		rep[4] = 0x00;
	}

	return 0;
}

static cmd_ret_t oem_test_getlm75(msg_header_t *header, unsigned char *rep, unsigned char *compcode) {
	int ret;
	unsigned char addr = main_ring_get();
	unsigned short ch_addr = 0x0200 | addr;
        unsigned char data[2];

	debug_printf("Test Sensor interface (%02xh address) \n", ch_addr);

	ret = i2c_io_ext(i2c_masteronly_channel_from_address(ch_addr), ch_addr
			| I2C_START | I2C_STOP, NULL, 0);
	if ((I2C_OK != i2c_get_last_error(i2c_masteronly_channel_from_address(
			ch_addr))) || (0 != ret)) {
		debug_printf("\t-> Error \n");
		rep[4] = 0xFF;
	} else {
		debug_printf("\t-> Done \n");
		rep[4] = 0x00;
	}

    if (i2c_dev_read_reg(addr, 0x00, data, 2)) {
		/* the sensor does not respond: set unreal temperature */
		rep[5] = 0xff;
    }

    rep[5] = data[0];

	return 0;
}

MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_SET_USERIO, cmd_prefix, 3, CMD_FL_NONE, 4, 4, oem_set_userio);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_GET_USERIO, cmd_prefix, 3, CMD_FL_NONE, 4, 5, oem_get_userio);

MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_SET_IPMIO, cmd_prefix, 3, CMD_FL_NONE, 4, 4, oem_set_ipmio);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_GET_IPMIO, cmd_prefix, 3, CMD_FL_NONE, 4, 5, oem_get_ipmio);

MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_SET_IPMCSN, cmd_prefix, 3, CMD_FL_NONE, 12, 4, oem_set_sn);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_GET_IPMCSN, cmd_prefix, 3, CMD_FL_NONE, 3, 13, oem_get_sn);

MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_SET_IPMCBN, cmd_prefix, 3, CMD_FL_NONE, 12, 4, oem_set_bn);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_GET_IPMCBN, cmd_prefix, 3, CMD_FL_NONE, 3, 13, oem_get_bn);

MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_SET_MACADDR, cmd_prefix, 3, CMD_FL_NONE, 9, 4, oem_set_macaddr);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_GET_MACADDR, cmd_prefix, 3, CMD_FL_NONE, 3, 10, oem_get_macaddr);

MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_CLR_USERIO, cmd_prefix, 3, CMD_FL_NONE, 4, 4, oem_clr_userio);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_CLR_IPMIO, cmd_prefix, 3, CMD_FL_NONE, 4, 4, oem_clr_ipmio);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_TEST_MGT_I2C, cmd_prefix, 3, CMD_FL_NONE, 6, 5, oem_test_mgti2c);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_TEST_SENSOR_I2C, cmd_prefix, 3, CMD_FL_NONE, 4, 5, oem_test_sensori2c);
MASTER_CMD_HANDLER(IPMI_NETFN_OEM_GROUP, OEM_TEST_GETLM75, cmd_prefix, 3, CMD_FL_NONE, 4, 6, oem_test_getlm75);


