#!/usr/bin/env python
# encoding: utf-8
from lxml import html
from colorama import init

import shutil
import os
import zipfile
import requests
import getpass
import sys
import urllib3
import argparse

#Initialization
init()

USERNAME = ''
PASSWORD = ''
ARGS = {}
    
class CERNSSORequest:
    
    def __init__(self, login_url, return_url, username, passwd):
        self.session = requests.Session()

        # get the login from web application = redirect to SSO (OpenID provider = keycloak)
        result = self.session.get(login_url)
        
        # retrieve the action attribute from the kc-form-login: it contains the submit URL
        tree = html.fromstring(result.text)
        action_url = tree.xpath("//form[@id='kc-form-login']/@action")[0]

        # create the payload, i.e. fill the login form
        payload = {
            "username": username, 
            "password": passwd, 
        }
        result = self.session.post(action_url, data=payload)
        if result.status_code != 200:
            raise ValueError("NetworkError: HTTP status code {}".format(result.status_code))

        if vars(ARGS)['2fa']:
            # retrieve the action attribute from the kc-otp-form-login: it contains the submit URL
            tree = html.fromstring(result.text)
            try:
                action_url = tree.xpath("//form[@id='kc-otp-login-form']/@action")[0]
            except IndexError:
                raise ValueError("LoginError: wrong username / password")

            otp = input("One-time code: ")
            payload = {
                "otp": otp,
                "login": "Sign+In",
            }
            result = self.session.post(action_url, data=payload)
            if result.status_code != 200:
                raise ValueError("NetworkError: HTTP status code {}".format(result.status_code))

        if result.url != return_url:
            raise ValueError("LoginError: wrong username / password")

    def get(self, url):
        return requests.get(url, cookies=self.session.cookies)
        
    def post(self, url, data, files = None):
        if files != None:
            return requests.post(url, cookies=self.session.cookies, files=files, data=data)
            
        else:
            return requests.post(url, cookies=self.session.cookies, data=data)
    
    def close(self):
        self.session.close()
        
    def post_stream(self, url, data, files = None):
        strVar = ''
        
        if files != None:
            r = requests.post(url, cookies=self.session.cookies, files=files, data=data, stream=True)
            
            strVar = ''
            countLoadPoints = 0
            
            for chunk in r.iter_content(chunk_size=1): 
                if chunk: # filter out keep-alive new chunks
                
                    countLoadPoints = countLoadPoints + 1
                    if countLoadPoints > 10:
                        sys.stdout.write('\r                   \rLoading ')
                        sys.stdout.flush()
                        countLoadPoints = 0
                        
                    sys.stdout.write('.')
                    sys.stdout.flush()
                    
                    strVar = strVar + chunk.decode("utf-8", "ignore")
                    split = strVar.split('\n', 1)
                    if len(split) > 1:
                        strVar = split[1]
                        if split[0].find("<a href='"):
                            sys.stdout.write('\r                   \r')
                            sys.stdout.flush()
                            countLoadPoints = 0
                            print(split[0].split(",")[0].replace('\n','').replace('\r','').replace("$",""))
                            up = split[0][split[0].find("<a href='")+9:]
                            link = 'https://cern-ipmc.web.cern.ch'+ up[:up.find("'")]
                        else:
                            sys.stdout.write('\r                   \r')
                            sys.stdout.flush()
                            countLoadPoints = 0
                            print(split[0].replace('\n','').replace('\r','').replace("$",""))

        else:
            r = requests.post(url, cookies=self.session.cookies, data=data, stream=True)
            
            strVar = ''
            countLoadPoints = 0
            
            for chunk in r.iter_content(chunk_size=1): 
                if chunk: # filter out keep-alive new chunks
                
                    countLoadPoints = countLoadPoints + 1
                    if countLoadPoints > 10:
                        sys.stdout.write('\r                   \rLoading ')
                        sys.stdout.flush()
                        countLoadPoints = 0
                        
                    sys.stdout.write('.')
                    sys.stdout.flush()
                    
                    strVar = strVar + chunk
                    split = strVar.split('\n', 1)
                    if len(split) > 1:
                        strVar = split[1]
                        if split[0].find("<a href='"):
                            sys.stdout.write('\r                   \r')
                            sys.stdout.flush()
                            countLoadPoints = 0
                            print(split[0].split(",")[0].replace('\n','').replace('\r','').replace("$",""))
                            up = split[0][split[0].find("<a href='")+9:]
                            link = 'https://cern-ipmc.web.cern.ch'+ up[:up.find("'")]
                        else:
                            sys.stdout.write('\r                   \r')
                            sys.stdout.flush()
                            countLoadPoints = 0
                            print(split[0].replace('\n','').replace('\r','').replace("$",""))
        
        sys.stdout.write('\r                   \r')
        print(strVar.replace("$",""))
        print("\033[0m")
        
        if 'downloadbin/' in link:
            print("\033[1m--> Binaries: {}\033[0m".format(link))
            return link
        else:
            print("\033[31;1m--> Compilation error \033[0m")
            return None
        
        
def zipdir(path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ziph.write(os.path.join(root, file), file)
                
def main(USERNAME, PASSWORD):

    LOGIN_URL = 'https://cern-ipmc.web.cern.ch/connect'
    RETURN_URL = 'https://cern-ipmc.web.cern.ch/'
    COMPILE_URL = 'https://cern-ipmc.web.cern.ch/compileProcess'
    if sys.version_info[0] < 2 or sys.version_info[0] >3 or sys.version_info[0] == 2 and sys.version_info[1] < 4:
        print ("\n\tERROR: Python 2.xx (2.4 or higher) is required to run this program.\n\tYou can download Python from http://www.python.org") 
        sys.exit(-1)
    
    global ARGS
    ap = argparse.ArgumentParser()
    ap.add_argument('-2','--2fa',help='use two-factor authentication',action='store_true');
    ARGS = ap.parse_args()

    if USERNAME == '':
        if sys.version_info[0] >=3:
            USERNAME = input("CERN Username: ") 
        else:
            USERNAME = raw_input("CERN Username: ")
    else:
        print('CERN Username: {}'.format(USERNAME))
        
    if PASSWORD == '':
        PASSWORD = getpass.getpass(prompt='CERN Password: ')

    try:
        CERNSSORequestInstance = CERNSSORequest(LOGIN_URL, RETURN_URL, USERNAME, PASSWORD)
    except ValueError as e:
        print(e)
        exit(-1)

    if os.path.exists('./.tmp') and os.path.isdir('./.tmp'):
        shutil.rmtree('./.tmp')
    
    os.mkdir('./.tmp')
    
    ipmcConfigFile = ''
    
    if os.path.exists('./ipmc-config') and os.path.isdir('./ipmc-config'):    
        zipf = zipfile.ZipFile('./.tmp/ipmc-config.zip', 'w', zipfile.ZIP_DEFLATED)
        zipdir('./ipmc-config', zipf)
        zipf.close()
        
        ipmcConfigFile = './.tmp/ipmc-config.zip'
    else:
        ipmcConfigFile = './config.xml'
        
    zipf = zipfile.ZipFile('./.tmp/ipmc-sensors.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir('./ipmc-sensors', zipf)
    zipf.close()
    zipf = zipfile.ZipFile('./.tmp/ipmc-user.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir('./ipmc-user', zipf)
    zipf.close()
    
    ipmcConfigFile_ptr = open(ipmcConfigFile, 'rb')
    ipmcSensorZipFile_ptr = open('./.tmp/ipmc-sensors.zip', 'rb')
    ipmcUserZipFile_ptr = open('./.tmp/ipmc-user.zip', 'rb')
    
    files = {'ipmcConfigZipFile': ipmcConfigFile_ptr, 'ipmcSensorZipFile': ipmcSensorZipFile_ptr, 'ipmcUserZipFile': ipmcUserZipFile_ptr}
    values = {'method': 'advanced', 'dest': 'cliTool'}

    for i in range(len(sys.argv)):
        if sys.argv[i] == '--commit':
            try:
                values['commit'] = sys.argv[i+1]
            except Exception:
                print("\033[1;31mUsage error: please specify the commit hash when --commit is specified \033[0m")

    link = CERNSSORequestInstance.post_stream(COMPILE_URL, data = values, files = files)    
    
    if link != None:        
        
        print("\033[1m--> Download and extract hpm.1 image \033[0m")
        outzip = open('./.tmp/ipmc-binaries.zip', 'wb+')
        result = CERNSSORequestInstance.get(link)
        outzip.write(result.content)
        outzip.close()
        
        zip_ref = zipfile.ZipFile('./.tmp/ipmc-binaries.zip', 'r')
        zip_ref.extractall('./.tmp/ipmc-binaries')
        zip_ref.close()
        
        sourcefiles = './.tmp/ipmc-binaries'
        destinationpath = './'
        for file in os.listdir(sourcefiles):
            if file.endswith('.img') or file.endswith('.ihx'):
                if os.path.exists(os.path.join(destinationpath,file)):
                    os.remove(os.path.join(destinationpath,file))
                os.rename(os.path.join(sourcefiles,file), os.path.join(destinationpath,file))
        
        CERNSSORequestInstance.close()   

    ipmcConfigFile_ptr.close()
    ipmcSensorZipFile_ptr.close()
    ipmcUserZipFile_ptr.close()
    
    if os.path.exists('./.tmp') and os.path.isdir('./.tmp'):
        shutil.rmtree('./.tmp')
    
if __name__ == '__main__':
    main(USERNAME, PASSWORD)
