/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the TMP431 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>
#include <debug.h>

#ifdef CFG_SENSOR_TMP431

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_tmp431.h>
#include <i2c_dev.h>

#include <sensor_pca9545.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use TMP431 sensors.
#endif

#undef DEBUG
// #ifndef DEBUG
// #define DEBUG
// #endif

//#define SNS_I2C_BUS         2                                           /* sensor I2C bus number */
#define TMP431_I2C_ADDR   MO_CHANNEL_ADDRESS(SENSOR_I2C_BUS, 0x4C << 1) /* I2C address of the TMP431A temperature sensor device */

/* ------------------------------------------------------------------ */

/* TMP431 Register definitions */
#define TMP431_LOCAL_TEMP_HIGH_REG		0x00
#define TMP431_LOCAL_TEMP_LOW_REG		0x15
#define TMP431_REMOTE_TEMP_HIGH_REG		0x01
#define TMP431_REMOTE_TEMP_LOW_REG		0x10
// #define TMP431_CFG_REG		1
// #define TMP431_THYST_REG	2
// #define TMP431_TOTI_REG		3
// #define TMP431_ADC_REG		4
// #define TMP431_CFG2_REG		5

// #define TMP431_CFG_SD		(1 << 0)	/* shutdown bit */
// #define TMP431_CFG_POL		(1 << 2)	/* OTI polarity bit */
// #define TMP431_CFG_CHAN(x)	((x)<<5)	/* channel selection */

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_tmp431_init(sensor_t *sensor);
static char sensor_tmp431_fill_reading(sensor_t *sensor, unsigned char *msg);

/* ------------------------------------------------------------------ */

/* TMP431 temperature sensor methods */
sensor_methods_t PROGMEM sensor_tmp431_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_tmp431_fill_reading,
    rearm:			&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:			&sensor_tmp431_init
};

/* Read-only info structures of TMP431 temperature sensors */
static const sensor_tmp431_ro_t PROGMEM sensor_tmp431_ro[] = { CFG_SENSOR_TMP431 };

#define TMP431_COUNT	sizeofarray(sensor_tmp431_ro)

/* Read-write info structures of TMP431 temperature sensors */
static struct sensor_tmp431 {
    sensor_threshold_t	sensor;
} sensor_tmp431[TMP431_COUNT] WARM_BSS;
typedef struct sensor_tmp431 sensor_tmp431_t;

DECL_SENSOR_GROUP(master, sensor_tmp431_ro, sensor_tmp431, NULL);

/* Global flags for all TMP431 sensors */
static unsigned char sensor_tmp431_global_flags;
#define TMP431_GLOBAL_INIT		(1 << 0)	/* initialize all sensors */
#define TMP431_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */
/*
    Internal functions for accessing TMP431
*/
/* ------------------------------------------------------------------ */

/* Read TMP431 temperature register */
static char inline sensor_tmp431_read_temp(unsigned char remote, unsigned char *temp)
{
    //unsigned char temp_high, temp_low;
	unsigned char data[2];
	
	*temp = 0xFF; // unrealistic value to mark an error
    //if (i2c_dev_read_reg(SENSOR_TMP431_ADDR, (remote) ? TMP431_REMOTE_TEMP_HIGH_REG: TMP431_LOCAL_TEMP_HIGH_REG, &temp_high, 1)) return I2C_ERROR;
    //if (i2c_dev_read_reg(SENSOR_TMP431_ADDR, (remote) ? TMP431_REMOTE_TEMP_LOW_REG: TMP431_LOCAL_TEMP_LOW_REG, &temp_high, 1)) return I2C_ERROR;
    if (i2c_dev_read_reg(TMP431_I2C_ADDR, (remote) ? TMP431_REMOTE_TEMP_HIGH_REG : TMP431_LOCAL_TEMP_HIGH_REG, data, 2)) return I2C_ERROR;
	// Resolution is 0.5 degC
    *temp = (data[0] << 1) | ((data[1] >> 7) & 1);
#ifdef DEBUG
    debug_printf("TMP431 @ %03x, data = %02x%02x, temp. = %d\n", TMP431_I2C_ADDR, data[0], data [1], *temp);
#endif
    return 0;
}
/*
    The following function updates the reading of the given TMP431 sensor.
*/
static void sensor_tmp431_update_reading(unsigned char num, unsigned char flags){
	unsigned char temp;

    /* get the I2C address of TMP431, and channel number */
	unsigned char slv_bus = (PRG_RD(sensor_tmp431_ro[num].bus_sns) >> 4) & 3;
	unsigned char remote = PRG_RD(sensor_tmp431_ro[num].bus_sns) & 1;
	
    if (monly_i2c_is_ready(TMP431_I2C_ADDR)) {

		/* select I2C switch slave channel */
		if (sensor_pca9545_write(1 << slv_bus) != I2C_OK) {
			debug_printf(PSTR("Failed to write to PCA9545A I2C switch\n"));
			return;
		}
		/* read the temperature value */
    	if (!sensor_tmp431_read_temp(remote, &temp))
			sensor_threshold_update_s(&sensor_tmp431[num].sensor, temp, flags);
		else
			debug_printf(PSTR("Error reading TMP431\n"));
    }
}

/*
    The following function initializes the given
    TMP431 sensor.
*/
static void sensor_tmp431_initialize(unsigned char num)
{
    /* get the I2C bus number of the PCA9545A and the remote local slect bits */
    //unsigned char bus_sns = PRG_RD(sensor_tmp431_ro[num].bus_sns);
#ifdef DEBUG
    debug_printf("TMP431 #%d, initialize\n", num);
#endif

    if (monly_i2c_is_ready(TMP431_I2C_ADDR)) {
		/* configure TMP431, nothing to be done */
    	//i2c_dev_write_reg(addr, 0x01, data, 1);
		//#ifdef DEBUG
    	//debug_printf(PSTR("Done \n"));
		//#endif
		/* read the current temperature/adc */
		sensor_tmp431_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
}

/*
    Sensor initialization.
*/
static char sensor_tmp431_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_tmp431 *) sensor) - sensor_tmp431;
    sensor_tmp431_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

#ifdef NEED_SLAVE_CALLBACKS
	SLAVE_UP_CALLBACK(sensor_tmp431_slave_up)
	{
		/* schedule global TMP431 init/update */
		sensor_tmp431_global_flags = TMP431_GLOBAL_INIT | TMP431_GLOBAL_UPDATE;
	}

	SLAVE_DOWN_CALLBACK(sensor_tmp431_slave_down)
	{
		/* unschedule global TMP431 init/update */
		sensor_tmp431_global_flags = 0;
	}
#endif

#ifdef NEED_CARRIER_CALLBACKS
	CARRIER_UP_CALLBACK(sensor_tmp431_carrier_up)
	{
		/* schedule global TMP431 init/update */
		sensor_TMP431_global_flags = TMP431_GLOBAL_INIT | TMP431_GLOBAL_UPDATE;
	}

	CARRIER_DOWN_CALLBACK(sensor_tmp431_carrier_down)
	{
		/* unschedule global TMP431 init/update */
		sensor_tmp431_global_flags = 0;
	}
#endif

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the TMP431 driver.
*/

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_tmp431_1s_callback)
{
    unsigned char flags;

    /* schedule global TMP431 update */
    save_flags_cli(flags);
    sensor_tmp431_global_flags |= TMP431_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_tmp431_poll)
{
    unsigned char i, flags, gflags;

    /* get/clear global TMP431 flags */
    save_flags_cli(flags);
    gflags = sensor_tmp431_global_flags;
    sensor_tmp431_global_flags = 0;
    restore_flags(flags);

    if (gflags & TMP431_GLOBAL_INIT) {
		/* make a delay to let the slave/carrier AVRs stabilize */
		udelay(20000);

			/* initialize all TMP431 */
		for (i = 0; i < TMP431_COUNT; i++) {
			if (!(sensor_tmp431[i].sensor.s.status & STATUS_NOT_PRESENT)) {
				sensor_tmp431_initialize(i);
			}
		}
    }

    if (gflags & TMP431_GLOBAL_UPDATE) {
		/* update all sensor readings */
		for (i = 0; i < TMP431_COUNT; i++) {
			if (!(sensor_tmp431[i].sensor.s.status & STATUS_NOT_PRESENT)) {
				sensor_tmp431_update_reading(i, 0);
			}
		}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_tmp431_init_all)
{
    /* schedule global initialization */
    sensor_tmp431_global_flags = TMP431_GLOBAL_INIT | TMP431_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains TMP431 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_tmp431_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_tmp431_update_reading((sensor_tmp431_t *)sensor - sensor_tmp431, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_TMP431 */
