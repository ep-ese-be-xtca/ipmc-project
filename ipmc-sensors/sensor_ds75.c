/*
    Copyright (c) 2003-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the DS75 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

#ifdef CFG_SENSOR_DS75

#include <hal/system.h>
#include <hal/irq.h>
#include <hal/i2c.h>
#include <hal/time.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_ds75.h>
#include <i2c_dev.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use DS75 sensors.
#endif

/* ------------------------------------------------------------------ */

/* DS75 register definitions */
#define DS75_TEMP_REG		0
#define DS75_CFG_REG		1
#define DS75_THYST_REG		2
#define DS75_TOS_REG		3

#define DS75_CFG_SD		(1 << 0)	/* Shutdown bit. */
#define DS75_CFG_TM		(1 << 1)	/* Thermostat mode. */
#define DS75_CFG_POL		(1 << 2)	/* O.S. Polarity Bit. */

/* O.S. Fault Tolerance bits. */
#define DS75_CFG_FT_1		(0 << 3)	/* 1 fault */
#define DS75_CFG_FT_2		(1 << 3)	/* 2 faults */
#define DS75_CFG_FT_4		(2 << 3)	/* 4 faults */
#define DS75_CFG_FT_6		(3 << 3)	/* 6 faults */

/* Thermometer Resolution Configuration */
#define DS75_CFG_RES_9		(0 << 5)	/* 9-bit */
#define DS75_CFG_RES_10		(1 << 5)	/* 10-bit */
#define DS75_CFG_RES_11		(2 << 5)	/* 11-bit */
#define DS75_CFG_RES_12		(3 << 5)	/* 12-bit */

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_ds75_set_thresholds(sensor_t *sensor,
	unsigned char mask, unsigned char *thresholds);
static char sensor_ds75_set_hysteresis(sensor_t *sensor,
	unsigned char *hysteresis);
static char sensor_ds75_fill_reading(sensor_t *sensor,
	unsigned char *msg);
static char sensor_ds75_init(sensor_t *sensor);

/* ------------------------------------------------------------------ */

/* DS75 temperature sensor methods */
sensor_methods_t PROGMEM sensor_ds75_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_ds75_fill_reading,
    rearm:		&sensor_threshold_rearm,
    set_thresholds:	&sensor_ds75_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_ds75_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:		&sensor_ds75_init
};

/* Read-only info structures of DS75 temperature sensors */
static const sensor_ds75_ro_t PROGMEM sensor_ds75_ro[] = { CFG_SENSOR_DS75 };

#define DS75_COUNT sizeofarray(sensor_ds75_ro)

/* Read-write info structures of DS75 temperature sensors */
static struct sensor_ds75 {
    sensor_threshold_t	sensor;
    unsigned char	flags;
} sensor_ds75[DS75_COUNT] WARM_BSS;
typedef struct sensor_ds75 sensor_ds75_t;

static unsigned short ds75_first;
DECL_SENSOR_GROUP(master, sensor_ds75_ro, sensor_ds75, &ds75_first);

/* DS75 temperature sensor flags */
#define DS75_UPDATE		(1 << 0)	/* need to re-read the sensor */
#define DS75_INITIALIZED	(1 << 1)	/* the sensor is initialized */

/* Global flags for all DS75 sensors */
static unsigned char sensor_ds75_global_flags;
#define DS75_GLOBAL_INIT	(1 << 0)	/* initialize all sensors */
#define DS75_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */

/*
    Internal functions for accessing DS75
*/

/* Write the DS75 configuration register */
static char inline sensor_ds75_write_conf(unsigned short addr,
	unsigned char conf)
{
    return i2c_dev_write_reg(addr, DS75_CFG_REG, &conf, 1);
}

/* Write a DS75 temperature register (temp, Tos, or Thyst) */
static char inline sensor_ds75_write_temp(unsigned short addr,
	unsigned char reg, char temp)
{
    unsigned char data[2] = { temp, 0 };

    return i2c_dev_write_reg(addr, reg, data, 2);
}

/* Read a DS75 temperature register (temp, Tos, or Thyst) */
static char inline sensor_ds75_read_temp(unsigned short addr,
	unsigned char reg, unsigned char *temp)
{
    unsigned char data[2];

    if (i2c_dev_read_reg(addr, reg, data, 2)) {
	/* the sensor does not respond: set unreal temperature */
	*temp = -99;
	return -1;
    }

    *temp = data[0];

#ifdef DEBUG
    debug_printf("ds75 @%02x, temp = %02x \n ", addr >> 8, addr & 0xFF, data[0]);
#endif

    return 0;
}

/* ------------------------------------------------------------------ */

/*
    The following function updates the DS75
    hardware registers.
*/
static void sensor_ds75_update_limits(unsigned char num)
{
    unsigned char tos, thyst;
    unsigned short addr;
    sensor_ds75_t *ds75;

    ds75 = &sensor_ds75[num];
    addr = PRG_RD(sensor_ds75_ro[num].i2c_addr);

    if (monly_i2c_is_ready(addr)) {

	/* calculate Tos and Thyst values */
	tos = ds75->sensor.thresholds[4];
	thyst = ds75->sensor.thresholds[4] -
		ds75->sensor.hysteresis[0] - 1;

#ifdef DEBUG
	debug_printf(PSTR("ds75 #%02x, thyst = %02x, tos = %02x \n", num, thyst, tos);
#endif

	/* set the lower temperature limit (DGH upper critical) */
	sensor_ds75_write_temp(addr, DS75_THYST_REG, thyst);

	/* set the upper temperature limit (AGH upper critical) */
	sensor_ds75_write_temp(addr, DS75_TOS_REG, tos);
    }
}

/*
    The following function updates the reading
    of the given DS75 sensor.
*/
static void sensor_ds75_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char snum, reading;
    unsigned short addr;
    sensor_t *sensor;

    snum = ds75_first + num;
    sensor = sensor_get_struct(&master_sensor_set, snum);
    if (sensor == NULL) {
	return;
    }
    addr = PRG_RD(sensor_ds75_ro[num].i2c_addr);

    if (monly_i2c_is_ready(addr)) {
	/* Do not update non present sensors */
	if (sensor->status & STATUS_NOT_PRESENT) {
	    return;
	}

	/* read temperature */
#ifndef CFG_SENSOR_DS75_DYNAMIC
	if (!sensor_ds75_read_temp(addr, DS75_TEMP_REG, &reading)) {
	    /* update sensor reading */
	    sensor_threshold_update(&master_sensor_set, snum, reading, flags);
	}
#else
	if (sensor_ds75_read_temp(addr, DS75_TEMP_REG, &reading)) {
	    /* the sensor does not respond */
	    if (!(sensor->status & STATUS_SENSOR_DISABLE)) {
		log_preamble();
		debug_printf("disabling DS75 sensor at %02x%02x \n", addr >> 8, addr & 0xFF);
		sensor->status |= STATUS_SENSOR_DISABLE;
	    }
	} else {
	    if (sensor->status & STATUS_SENSOR_DISABLE) {
		log_preamble();
		debug_printf("enabling DS75 sensor at %02x%02x \n", addr >> 8, addr & 0xFF);
		sensor->status &= ~STATUS_SENSOR_DISABLE;
	    }
	    /* update sensor reading */
	    sensor_threshold_update(&master_sensor_set, snum, reading, flags);
	}
#endif
    }
}

/*
    The following function initializes the given
    DS75 sensor.
*/
static void sensor_ds75_initialize(unsigned char num)
{
    unsigned short addr;
    sensor_ds75_t *ds75;

    ds75 = &sensor_ds75[num];
    addr = PRG_RD(sensor_ds75_ro[num].i2c_addr);

    if (monly_i2c_is_ready(addr)) {
	/* skip initialization, if it is already initialized or not present */
	if ((ds75->flags & DS75_INITIALIZED) ||
		(ds75->sensor.s.status & STATUS_NOT_PRESENT)) {
	    return;
	}

#ifdef DEBUG
	debug_printf(PSTR("ds75 #%02x, initialize\n", num);
#endif

	/* Configure DS75:
	 *  - interrupt mode
	 *  - active low
	 *  - one fault to generate event
	 *  - 9-bit resolution
	 */
	sensor_ds75_write_conf(addr, DS75_CFG_TM |
		DS75_CFG_FT_1 | DS75_CFG_RES_9);

	/* update DS75 limits */
	sensor_ds75_update_limits(num);

	/* read the current temperature */
	sensor_ds75_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
}

/*
    Sensor initialization.
*/
static char sensor_ds75_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_ds75 *) sensor) - sensor_ds75;
    sensor_ds75_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the DS75 driver.
*/

/* Interrupt handler */
static void sensor_ds75_intr(unsigned char irq)
{
    unsigned char i;

#ifdef DEBUG
    debug_printf("interrupt #%02x \n", irq);
#endif

    /* temporary block irq */
    irq_block(irq);

    for (i = 0; i < DS75_COUNT; i++) {
	if (PRG_RD(sensor_ds75_ro[i].irq) == irq) {
	    sensor_ds75[i].flags |= DS75_UPDATE;
	}
    }
}

#ifdef NEED_SLAVE_CALLBACKS
/* Slave up callback */
SLAVE_UP_CALLBACK(sensor_ds75_slave_up)
{
    /* schedule global DS75 init */
    sensor_ds75_global_flags |= DS75_GLOBAL_INIT;
}

/* Slave down callback */
SLAVE_DOWN_CALLBACK(sensor_ds75_slave_down)
{
    unsigned char i;

    /* unschedule global DS75 init */
    sensor_ds75_global_flags &= ~DS75_GLOBAL_INIT;

    for (i = 0; i < DS75_COUNT; i++) {
	/* mark the sensor as not initialized */
	sensor_ds75[i].flags &= ~DS75_INITIALIZED;

	/* disable interrupts from this sensor */
	if (PRG_RD(sensor_ds75_ro[i].irq) < IRQ_NUM) {
	    irq_block(PRG_RD(sensor_ds75_ro[i].irq));
	}
    }
}
#endif

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_ds75_1s_callback)
{
    unsigned char flags;

    /* schedule global DS75 update */
    save_flags_cli(flags);
    sensor_ds75_global_flags |= DS75_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_ds75_poll)
{
    unsigned char i, flags, gflags, irq;
    sensor_ds75_t *ds75;

    /* get/clear global DS75 flags */
    save_flags_cli(flags);
    gflags = sensor_ds75_global_flags;
    sensor_ds75_global_flags = 0;
    restore_flags(flags);

    if (gflags & DS75_GLOBAL_INIT) {
	/* make a delay to let the slave (if any) stabilize */
	udelay(20000);

        /* initialize all DS75 */
	for (i = 0; i < DS75_COUNT; i++) {
    	    sensor_ds75_initialize(i);
	}

	/* acknowledge all interrupts */
	for (i = 0; i < DS75_COUNT; i++) {
    	    irq = PRG_RD(sensor_ds75_ro[i].irq);
	    if (irq < IRQ_NUM) {
    		irq_ack(irq);
	    }
	}

	/* mark all DS75 as initialized and */
	/* schedule their updates */
	save_flags_cli(flags);
	for (i = 0; i < DS75_COUNT; i++) {
	    sensor_ds75[i].flags = DS75_INITIALIZED | DS75_UPDATE;
	}
	restore_flags(flags);
    }

    for (i = 0; i < DS75_COUNT; i++) {
	ds75 = &sensor_ds75[i];

	/* check if sensor update is needed */
	if ((gflags & DS75_GLOBAL_UPDATE) || (ds75->flags & DS75_UPDATE)) {
	    /* clear the update flag */
	    save_flags_cli(flags);
	    ds75->flags &= ~DS75_UPDATE;
	    restore_flags(flags);

	    /* update sensor reading */
	    sensor_ds75_update_reading(i, 0);
	}
    }

    /* enable all interrupts */
    for (i = 0; i < DS75_COUNT; i++) {
        irq = PRG_RD(sensor_ds75_ro[i].irq);
	if (irq < IRQ_NUM) {
    	    irq_unblock(irq);
	}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_ds75_init_callback)
{
    unsigned char i, irq, flags;
    BITMAP_INIT(irq_mask, IRQ_NUM);

    for (i = 0; i < DS75_COUNT; i++) {
	irq  = PRG_RD(sensor_ds75_ro[i].irq);

	/* Configure irq:
	 *  - block irq
	 *  - low level for interrupt
	 *  - register interrupt handler
	 */
	if (irq < IRQ_NUM && !BITMAP_ISSET(irq_mask, irq)) {
	    irq_block(irq);
	    irq_conf(irq, IRQ_LOW_LEVEL);
	    irq_register(irq, sensor_ds75_intr);
	    BITMAP_SET(irq_mask, irq);
	}
    }

    /* schedule global initialization */
    save_flags_cli(flags);
    sensor_ds75_global_flags |= DS75_GLOBAL_INIT;
    restore_flags(flags);
}

/* ------------------------------------------------------------------ */

/*
    This section contains DS75 sensor methods.
*/

/* Set sensor thresholds */
static char sensor_ds75_set_thresholds(sensor_t *sensor,
	unsigned char mask, unsigned char *thresholds)
{
    /* set thesholds */
    sensor_threshold_set_thresholds(sensor, mask, thresholds);

    /* update limits */
    sensor_ds75_update_limits(((sensor_ds75_t*)sensor) - sensor_ds75);

    return 0;
}

/* Set sensor hysteresis */
static char sensor_ds75_set_hysteresis(sensor_t *sensor,
	unsigned char *hysteresis)
{
    /* set hysteresis */
    sensor_threshold_set_hysteresis(sensor, hysteresis);

    /* update limits */
    sensor_ds75_update_limits(((sensor_ds75_t*)sensor) - sensor_ds75);

    return 0;
}

/* Fill the Get Sensor Reading reply */
static char sensor_ds75_fill_reading(sensor_t *sensor,
	unsigned char *msg)
{
    /* update current reading */
    sensor_ds75_update_reading(((sensor_ds75_t *)sensor) - sensor_ds75, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_DS75 */
