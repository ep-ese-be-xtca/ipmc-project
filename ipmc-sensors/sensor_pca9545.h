#ifndef __SENSOR_PCA9545_H__
#define __SENSOR_PCA9545_H__

#include <hal/i2c.h>
#include <app/serial.h>

#define SENSOR_I2C_BUS     2                                             /* sensor I2C bus number */
#define PCA9545_I2C_ADDR   MO_CHANNEL_ADDRESS(SENSOR_I2C_BUS, 0x70 << 1) /* I2C address of the PCA9545A I2C switch */

/* Write the PCA9545A I2C switch slave select register */
static char inline sensor_pca9545_write(unsigned char mask)
{
	int ret = i2c_io(PCA9545_I2C_ADDR | I2C_START | I2C_STOP, &mask, 1);
#ifdef DEBUG
	debug_printf("Write PCA9545A I2C mux at 0x%03x, data: 0x%02x, stats: %d\n", addr, mask, ret);
#endif
    return (ret < I2C_OK) ? (-1) : 0;
}

#endif /* __SENSOR_PCA9545_H__ */
