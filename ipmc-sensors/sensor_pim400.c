/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the PIM400 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

#ifdef CFG_SENSOR_PIM400

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_pim400.h>
#include <i2c_dev.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use PIM400 sensors.
#endif

#undef DEBUG
//#define DEBUG

/* ------------------------------------------------------------------ */

#define MGMT_I2C_BUS        1
#define PIM400_I2C_ADDR     MO_CHANNEL_ADDRESS(MGMT_I2C_BUS, 0x2F << 1) /* I2C address of the PIM400KZ module */

/* PIM400 Register definitions */
#define PIM400_HLDP_VOLT_REG		0x1F
#define PIM400_M48V_IOUT_REG		0x21
#define PIM400_M48V_AF_REG          0x22
#define PIM400_M48V_BF_REG          0x23
#define PIM400_TEMP_REG	            0x28

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_pim400_init(sensor_t *sensor);
static char sensor_pim400_fill_reading(sensor_t *sensor, unsigned char *msg);

/* ------------------------------------------------------------------ */

/* PIM400 temperature sensor methods */
sensor_methods_t PROGMEM sensor_pim400_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_pim400_fill_reading,
    rearm:			&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:			&sensor_pim400_init
};

/* Read-only info structures of PIM400 temperature sensors */
static const sensor_pim400_ro_t PROGMEM sensor_pim400_ro[] = { CFG_SENSOR_PIM400 };

#define PIM400_COUNT	sizeofarray(sensor_pim400_ro)

/* Read-write info structures of PIM400 temperature sensors */
static struct sensor_pim400 {
    sensor_threshold_t	sensor;
} sensor_pim400[PIM400_COUNT] WARM_BSS;
typedef struct sensor_pim400 sensor_pim400_t;

DECL_SENSOR_GROUP(master, sensor_pim400_ro, sensor_pim400, NULL);

/* Global flags for all PIM400 sensors */
static unsigned char sensor_pim400_global_flags;
#define PIM400_GLOBAL_INIT	    (1 << 0)	/* initialize all sensors */
#define PIM400_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */
/*    Internal functions for accessing PIM400                        */
/* ------------------------------------------------------------------ */

/* Read an PIM400 register */
static char inline sensor_pim400_read_sensor(unsigned char reg, unsigned char *val)
{
	*val = 0xFF; // unrealistic value to mark an error
    if (i2c_dev_read_reg(PIM400_I2C_ADDR, reg, val, 1)) return I2C_ERROR;
#ifdef DEBUG
    debug_printf("PIM400KZ @ 0x%03x, register 0x%02x: data = %d\n", PIM400_I2C_ADDR, reg, *val);
#endif
    return 0;
}

/* ------------------------------------------------------------------ */

/* 
    The following function updates the reading of the given PIM400 sensor. 
*/
static void sensor_pim400_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char reading;
    /* get the sensor number */
	/* get the channel number */
    unsigned char chan = PRG_RD(sensor_pim400_ro[num].chan) & 7;
	unsigned char reg_map[5] = { PIM400_HLDP_VOLT_REG, PIM400_M48V_IOUT_REG, PIM400_M48V_AF_REG, PIM400_M48V_BF_REG, PIM400_TEMP_REG };
    if (monly_i2c_is_ready(PIM400_I2C_ADDR)) {
		/* read the sensor value */
		if (!sensor_pim400_read_sensor(reg_map[chan], &reading)) {
			/* update sensor reading */
			sensor_threshold_update_s(&sensor_pim400[num].sensor, reading, flags);
		}
    }
}

/*
    The following function initializes the given PIM400 sensor.
*/
static void sensor_pim400_initialize(unsigned char num)
{
#ifdef DEBUG
    debug_printf("pim400 #%d, initialize\n", num);
#endif
    if (monly_i2c_is_ready(PIM400_I2C_ADDR)) {
		/* read the current sensor value */
		sensor_pim400_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
}

/*
    Sensor initialization.
*/
static char sensor_pim400_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_pim400 *) sensor) - sensor_pim400;
    sensor_pim400_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

#ifdef NEED_SLAVE_CALLBACKS
SLAVE_UP_CALLBACK(sensor_pim400_slave_up)
{
    /* schedule global PIM400 init/update */
    sensor_pim400_global_flags = PIM400_GLOBAL_INIT | PIM400_GLOBAL_UPDATE;
}

SLAVE_DOWN_CALLBACK(sensor_pim400_slave_down)
{
    /* unschedule global PIM400 init/update */
    sensor_pim400_global_flags = 0;
}
#endif

#ifdef NEED_CARRIER_CALLBACKS
CARRIER_UP_CALLBACK(sensor_pim400_carrier_up)
{
    /* schedule global PIM400 init/update */
    sensor_pim400_global_flags = PIM400_GLOBAL_INIT | PIM400_GLOBAL_UPDATE;
}

CARRIER_DOWN_CALLBACK(sensor_pim400_carrier_down)
{
    /* unschedule global PIM400 init/update */
    sensor_pim400_global_flags = 0;
}
#endif

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the PIM400 driver.
*/

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_pim400_1s_callback)
{
    unsigned char flags;

    /* schedule global PIM400 update */
    save_flags_cli(flags);
    sensor_pim400_global_flags |= PIM400_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_pim400_poll)
{
    unsigned char i, flags, gflags;

    /* get/clear global PIM400 flags */
    save_flags_cli(flags);
    gflags = sensor_pim400_global_flags;
    sensor_pim400_global_flags = 0;
    restore_flags(flags);

    if (gflags & PIM400_GLOBAL_INIT) {
	/* make a delay to let the slave/carrier AVRs stabilize */
	udelay(20000);

        /* initialize all PIM400 */
	for (i = 0; i < PIM400_COUNT; i++) {
	    if (!(sensor_pim400[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_pim400_initialize(i);
	    }
	}
    }

    if (gflags & PIM400_GLOBAL_UPDATE) {
	/* update all sensor readings */
	for (i = 0; i < PIM400_COUNT; i++) {
	    if (!(sensor_pim400[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_pim400_update_reading(i, 0);
	    }
	}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_pim400_init_all)
{
    /* schedule global initialization */
    sensor_pim400_global_flags = PIM400_GLOBAL_INIT | PIM400_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains PIM400 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_pim400_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_pim400_update_reading((sensor_pim400_t *)sensor - sensor_pim400, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_PIM400 */
