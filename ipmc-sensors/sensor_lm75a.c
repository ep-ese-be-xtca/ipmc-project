/*
    Copyright (c) 2003-2016 Pentair Technical Products, Inc.
    All rights reserved.
    Pentair Technical Products, Inc. proprietary and confidential.

    Description:
	This module implements the LM75A (originally DS75) sensors. The code has been derived from that for the very similar DS75

    $Revision: 13620 $

    Note: The copyright above refers to the file ds75.c. This file has been created my M. Joos, CERN, on the basis of ds75.c
*/


#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

//#define DEBUG

#ifdef CFG_SENSOR_LM75A

#include <hal/system.h>
#include <hal/irq.h>
#include <hal/i2c.h>
#include <hal/time.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_lm75a.h>
#include <i2c_dev.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use LM75A sensors.
#endif

/* ------------------------------------------------------------------ */

/* LM75A register definitions */
#define LM75A_TEMP_REG		0
#define LM75A_CFG_REG		1
#define LM75A_THYST_REG		2
#define LM75A_TOS_REG		3

#define LM75A_CFG_SD		(1 << 0)	/* Shutdown bit. */
#define LM75A_CFG_TM		(1 << 1)	/* Thermostat mode. */
#define LM75A_CFG_POL		(1 << 2)	/* O.S. Polarity Bit. */

/* O.S. Fault Tolerance bits. */
#define LM75A_CFG_FT_1		(0 << 3)	/* 1 fault */
#define LM75A_CFG_FT_2		(1 << 3)	/* 2 faults */
#define LM75A_CFG_FT_4		(2 << 3)	/* 4 faults */
#define LM75A_CFG_FT_6		(3 << 3)	/* 6 faults */

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_lm75a_set_thresholds(sensor_t *sensor, unsigned char mask, unsigned char *thresholds);
static char sensor_lm75a_set_hysteresis(sensor_t *sensor, unsigned char *hysteresis);
static char sensor_lm75a_fill_reading(sensor_t *sensor, unsigned char *msg);
static char sensor_lm75a_init(sensor_t *sensor);

/* ------------------------------------------------------------------ */

/* LM75A temperature sensor methods */
sensor_methods_t PROGMEM sensor_lm75a_methods = {
#ifdef CFG_SENSOR_EVENT_ENABLE
    fill_event:		&sensor_threshold_fill_event,
    rearm:		&sensor_threshold_rearm,
#endif
    fill_reading:	&sensor_lm75a_fill_reading,
    set_thresholds:	&sensor_lm75a_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_lm75a_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:		&sensor_lm75a_init
};

/* Read-only info structures of LM75A temperature sensors */
static const sensor_lm75a_ro_t PROGMEM sensor_lm75a_ro[] = { CFG_SENSOR_LM75A };

#define LM75A_COUNT sizeofarray(sensor_lm75a_ro)

/* Read-write info structures of LM75A temperature sensors */
static struct sensor_lm75a {
    sensor_threshold_t	sensor;
    unsigned char	flags;
} sensor_lm75a[LM75A_COUNT] WARM_BSS;
typedef struct sensor_lm75a sensor_lm75a_t;

DECL_SENSOR_GROUP(master, sensor_lm75a_ro, sensor_lm75a, NULL);

/* LM75A temperature sensor flags */
#define LM75A_UPDATE		(1 << 0)	/* need to re-read the sensor */
#define LM75A_INITIALIZED	(1 << 1)	/* the sensor is initialized */

/* Global flags for all LM75A sensors */
static unsigned char sensor_lm75a_global_flags;
#define LM75A_GLOBAL_INIT	(1 << 0)	/* initialize all sensors */
#define LM75A_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */

/*
    Internal functions for accessing LM75A
*/

/* Write the LM75A configuration register */
static char inline sensor_lm75a_write_conf(unsigned short addr,	unsigned char conf)
{
    return i2c_dev_write_reg(addr, LM75A_CFG_REG, &conf, 1);
}


/* Write a LM75A temperature register (Tos or Thyst) */
static char inline sensor_lm75a_write_temp(unsigned short addr,	unsigned char reg, char temp)
{
    unsigned char data[2] = { temp, 0 };                 //MJ: The LSB/D7 is ignored. Therefore the temperature resolution is 1 deg. C
    
    return i2c_dev_write_reg(addr, reg, data, 2);
}


/* Read a LM75A temperature register (temp, Tos, or Thyst) */
static char inline sensor_lm75a_read_temp(unsigned short addr, unsigned char reg, unsigned char *temp)
{
    unsigned char data[2];

    if (i2c_dev_read_reg(addr, reg, data, 2)) {   /* the sensor does not respond: set unreal temperature */
	*temp = -99;
	return -1;
    }

    *temp = data[0];   //MJ: The LSB/D7 is ignored. Therefore the temperature resolution is 1 deg. C

#ifdef DEBUG
    debug_puts_P(PSTR("lm75a @ "));
    debug_puthex8(addr >> 8);
    debug_puthex8(addr & 0xFF);
    debug_puts_P(PSTR(", temp = "));
    debug_putdec(data[0]);
    debug_putc('\n');
#endif

    return 0;
}

/* ------------------------------------------------------------------ */

/*
    The following function updates the LM75A hardware registers.
*/
static void sensor_lm75a_update_limits(unsigned char num)
{
    unsigned char tos, thyst;
    unsigned short addr;
    sensor_lm75a_t *lm75a;

    lm75a = &sensor_lm75a[num];
    addr = PRG_RD(sensor_lm75a_ro[num].i2c_addr);

    /* Do not update non present sensors */
    if (!(lm75a->flags & LM75A_INITIALIZED) || lm75a->sensor.s.status & STATUS_NOT_PRESENT) {
	return;
    }

    /* calculate Tos and Thyst values */
    tos   = lm75a->sensor.thresholds[4];
    thyst = lm75a->sensor.thresholds[4] - lm75a->sensor.hysteresis[0] - 1;

#ifdef DEBUG
    debug_puts_P(PSTR("lm75a # "));
    debug_puthex8(num);
    debug_puts_P(PSTR(", thyst = "));
    debug_puthex8(thyst);
    debug_puts_P(PSTR(", tos = "));
    debug_puthex8(tos);
    debug_putc('\n');
#endif

    /* set the lower temperature limit (DGH upper critical) */
    sensor_lm75a_write_temp(addr, LM75A_THYST_REG, thyst);

    /* set the upper temperature limit (AGH upper critical) */
    sensor_lm75a_write_temp(addr, LM75A_TOS_REG, tos);
}


/*
    The following function updates the reading of the given LM75A sensor.
*/
static void sensor_lm75a_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char reading;
    unsigned short addr;
    sensor_t *sensor;

    sensor = &sensor_lm75a[num].sensor.s;
    if (sensor == NULL) {
	return;
    }
    addr = PRG_RD(sensor_lm75a_ro[num].i2c_addr);

    /* Do not update non present sensors */
    if (sensor->status & STATUS_NOT_PRESENT) {
	return;
    }

    /* read temperature */
#ifndef CFG_SENSOR_LM75A_DYNAMIC
    if (!sensor_lm75a_read_temp(addr, LM75A_TEMP_REG, &reading)) {
	/* update sensor reading */
	sensor_threshold_update_s(&sensor_lm75a[num].sensor, reading, flags);
    }
#else
    if (sensor_lm75a_read_temp(addr, LM75A_TEMP_REG, &reading)) {
	/* the sensor does not respond */
	if (!(sensor->status & STATUS_SENSOR_DISABLE)) {
	    log_preamble();
	    log_puts_P(PSTR("disabling LM75A sensor at "));
	    log_puthex8(addr >> 8);
	    log_puthex8(addr & 0xFF);
	    log_putc('\n');
	    sensor->status |= STATUS_SENSOR_DISABLE;
	}
    } else {
	if (sensor->status & STATUS_SENSOR_DISABLE) {
	    log_preamble();
	    log_puts_P(PSTR("enabling LM75A sensor at "));
	    log_puthex8(addr >> 8);
	    log_puthex8(addr & 0xFF);
	    log_putc('\n');
	    sensor->status &= ~STATUS_SENSOR_DISABLE;
	}
	/* update sensor reading */
	sensor_threshold_update_s(&sensor_lm75a[num].sensor, reading, flags);
    }
#endif
}


/*
    The following function initializes the given LM75A sensor.
*/
static void sensor_lm75a_initialize(unsigned char num)
{
    unsigned short addr;
    sensor_lm75a_t *lm75a;

    lm75a = &sensor_lm75a[num];
    addr = PRG_RD(sensor_lm75a_ro[num].i2c_addr);

    /* skip initialization, if it is already initialized or not present */
    if ((lm75a->flags & LM75A_INITIALIZED) || (lm75a->sensor.s.status & STATUS_NOT_PRESENT)) {
	return;
    }

#ifdef DEBUG
    debug_puts_P(PSTR("lm75a # "));
    debug_puthex8(num);
    debug_puts_P(PSTR(", initialize\n"));
#endif

    /* Configure LM75A:
     *  - interrupt mode
     *  - active low
     *  - one fault to generate event
     */
    sensor_lm75a_write_conf(addr, LM75A_CFG_TM | LM75A_CFG_FT_1);

    /* update LM75A limits */
    sensor_lm75a_update_limits(num);

    /* read the current temperature */
    sensor_lm75a_update_reading(num, SENSOR_INITIAL_UPDATE);
}


/*
    Sensor initialization.
*/
static char sensor_lm75a_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_lm75a *) sensor) - sensor_lm75a;
    sensor_lm75a_initialize(num);
    return 0;
}


/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the LM75A driver.
*/

/* Interrupt handler */
static void sensor_lm75a_intr(unsigned char irq)
{
    unsigned char i;

#ifdef DEBUG
    debug_puts_P(PSTR("interrupt # "));
    debug_puthex8(irq);
    debug_putc('\n');
#endif

    /* temporary block irq */
    irq_block(irq);

    for (i = 0; i < LM75A_COUNT; i++) {
	if (PRG_RD(sensor_lm75a_ro[i].irq) == irq) {
	    sensor_lm75a[i].flags |= LM75A_UPDATE;
	}
    }
}


#ifdef NEED_SLAVE_CALLBACKS
/* Slave up callback */
SLAVE_UP_CALLBACK(sensor_lm75a_slave_up)
{
    /* schedule global LM75A init */
    sensor_lm75a_global_flags |= LM75A_GLOBAL_INIT;
}

/* Slave down callback */
SLAVE_DOWN_CALLBACK(sensor_lm75a_slave_down)
{
    unsigned char i;

    /* unschedule global LM75A init */
    sensor_lm75a_global_flags &= ~LM75A_GLOBAL_INIT;

    for (i = 0; i < LM75A_COUNT; i++) {
	/* mark the sensor as not initialized */
	sensor_lm75a[i].flags &= ~LM75A_INITIALIZED;

	/* disable interrupts from this sensor */
	if (PRG_RD(sensor_lm75a_ro[i].irq) < IRQ_NUM) {
	    irq_block(PRG_RD(sensor_lm75a_ro[i].irq));
	}
    }
}
#endif


/* 1 second callback */
TIMER_CALLBACK(1s, sensor_lm75a_1s_callback)
{
    unsigned char flags;

    /* schedule global LM75A update */
    save_flags_cli(flags);
    sensor_lm75a_global_flags |= LM75A_GLOBAL_UPDATE;
    restore_flags(flags);
}


/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_lm75a_poll)
{
    unsigned char i, flags, gflags, irq;
    sensor_lm75a_t *lm75a;

    /* get/clear global LM75A flags */
    save_flags_cli(flags);
    gflags = sensor_lm75a_global_flags;
    sensor_lm75a_global_flags = 0;
    restore_flags(flags);

    if (gflags & LM75A_GLOBAL_INIT) {
	/* make a delay to let the slave (if any) stabilize */
	udelay(20000);

        /* initialize all LM75A */
	for (i = 0; i < LM75A_COUNT; i++) {
	    sensor_lm75a_initialize(i);
	}

	/* acknowledge all interrupts */
	for (i = 0; i < LM75A_COUNT; i++) {
	    irq = PRG_RD(sensor_lm75a_ro[i].irq);
	    if (irq < IRQ_NUM) {
		irq_ack(irq);
	    }
	}

	/* mark all LM75A as initialized and schedule their updates */
	save_flags_cli(flags);
	for (i = 0; i < LM75A_COUNT; i++) {
	    sensor_lm75a[i].flags = LM75A_INITIALIZED | LM75A_UPDATE;
	}
	restore_flags(flags);
    }

    for (i = 0; i < LM75A_COUNT; i++) {
	lm75a = &sensor_lm75a[i];

	/* check if sensor update is needed */
	if ((gflags & LM75A_GLOBAL_UPDATE) || (lm75a->flags & LM75A_UPDATE)) {
	    /* clear the update flag */
	    save_flags_cli(flags);
	    lm75a->flags &= ~LM75A_UPDATE;
	    restore_flags(flags);

	    /* update sensor reading */
	    sensor_lm75a_update_reading(i, 0);
	}
    }

    /* enable all interrupts */
    for (i = 0; i < LM75A_COUNT; i++) {
        irq = PRG_RD(sensor_lm75a_ro[i].irq);
	if (irq < IRQ_NUM) {
	    irq_unblock(irq);
	}
    }
}


/* Initialization callback */
INIT_CALLBACK(sensor_lm75a_init_callback)
{
    unsigned char i, irq, flags;
    BITMAP_INIT(irq_mask, IRQ_NUM);

    for (i = 0; i < LM75A_COUNT; i++) {
	irq  = PRG_RD(sensor_lm75a_ro[i].irq);

	/* Configure irq:
	 *  - block irq
	 *  - low level for interrupt
	 *  - register interrupt handler
	 */
	if (irq < IRQ_NUM && !BITMAP_ISSET(irq_mask, irq)) {
	    irq_block(irq);
	    irq_conf(irq, IRQ_LOW_LEVEL);
	    irq_register(irq, sensor_lm75a_intr);
	    BITMAP_SET(irq_mask, irq);
	}
    }

    /* schedule global initialization */
    save_flags_cli(flags);
    sensor_lm75a_global_flags |= LM75A_GLOBAL_INIT;
    restore_flags(flags);
}

/* ------------------------------------------------------------------ */

/*
    This section contains LM75A sensor methods.
*/

/* Set sensor thresholds */
static char sensor_lm75a_set_thresholds(sensor_t *sensor, unsigned char mask, unsigned char *thresholds)
{
    /* set thesholds */
    sensor_threshold_set_thresholds(sensor, mask, thresholds);

    /* update limits */
    sensor_lm75a_update_limits(((sensor_lm75a_t*)sensor) - sensor_lm75a);

    return 0;
}


/* Set sensor hysteresis */
static char sensor_lm75a_set_hysteresis(sensor_t *sensor, unsigned char *hysteresis)
{
    /* set hysteresis */
    sensor_threshold_set_hysteresis(sensor, hysteresis);

    /* update limits */
    sensor_lm75a_update_limits(((sensor_lm75a_t*)sensor) - sensor_lm75a);

    return 0;
}


/* Fill the Get Sensor Reading reply */
static char sensor_lm75a_fill_reading(sensor_t *sensor,	unsigned char *msg)
{
    /* update current reading */
    sensor_lm75a_update_reading(((sensor_lm75a_t *)sensor) - sensor_lm75a, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_LM75A */
