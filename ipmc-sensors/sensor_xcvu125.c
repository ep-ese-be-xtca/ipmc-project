#define NEED_MASTERONLY_I2C			/* Check that the Sensor_i2c bus is implemented */ 
 
#include <defs.h> 
#include <cfgint.h> 
#include <debug.h> 
 
#ifdef CFG_SENSOR_XCVU125			/* Compile the source only if at least one SENSOR_XCVU125 is implemented by the user */ 
 
#include <hal/i2c.h>				/* I2C functions */ 
#include <i2c_dev.h>				/* Master Only I2C functions */ 
#include <hal/system.h>				/* System functions */ 
	 
#include <app.h>					/* App functions */ 
#include <log.h>					/* Log functions */ 
#include <sensor.h>					/* Sensors functions */ 
#include <sensor_discrete.h>		/* Discrete sensor functions */ 
#include <sensor_threshold.h>		/* Threshold related functions */ 
 
#include <sensor_xcvu125.h>			/* Sensor related header file */ 
 
#ifndef HAS_MASTERONLY_I2C 
    #error Enable master-only I2C support to use XCVU125 sensors. 
#endif 
 
 
/* ------------------------------------------------------------------ */ 
/* Sensor's methods 				 								  */ 
/* ------------------------------------------------------------------ */ 
 
/* Sensor specific methods */ 
static char sensor_xcvu125_init(sensor_t *sensor);		/* Function called by the core to initialize the device */ 
 
sensor_methods_t PROGMEM sensor_xcvu125_methods = { 
    fill_event:		&sensor_threshold_fill_event,		/* Called when the core asks for event */ 
    fill_reading:	&sensor_xcvu125_fill_rd,			/* Called when the core asks for sensor value */ 
    rearm:			&sensor_threshold_rearm,			/* Called when the core asks for sensor arm */ 
    set_thresholds:	&sensor_threshold_set_thresholds,	/* Called when a new threshold value is forced */ 
    get_thresholds:	&sensor_threshold_get_thresholds,	/* Called when thresholds value are requested */ 
    set_hysteresis:	&sensor_threshold_set_hysteresis,	/* Called to set a new threshold hysteresis value */ 
    get_hysteresis:	&sensor_threshold_get_hysteresis,	/* Called to get the threshold hysteresis value */ 
    init:			&sensor_xcvu125_init				/* Called when the core asks for sensor init */ 
}; 
 
/* ------------------------------------------------------------------ */ 
/* Memory allocation 												  */ 
/* ------------------------------------------------------------------ */ 
static const sensor_xcvu125_ro_t PROGMEM sensor_xcvu125_ro[] = { 
	CFG_SENSOR_XCVU125 /* Defined in impc-config/config_sensors.h */ 
}; 
 
#define SENSOR_XCVU125_COUNT	sizeofarray(sensor_xcvu125_ro) 
 
/* Read-write info structures of XCVU125 temperature sensors */ 
static struct sensor_xcvu125 { 
    sensor_threshold_t	sensor; 
} sensor_xcvu125[SENSOR_XCVU125_COUNT] WARM_BSS; 
 
typedef struct sensor_xcvu125 sensor_xcvu125_t; 
   
 
/* ------------------------------------------------------------------ */ 
/* Sensor declaration												  */ 
/* ------------------------------------------------------------------ */ 
DECL_SENSOR_GROUP(master, sensor_xcvu125_ro, sensor_xcvu125, NULL); 
 
/* ------------------------------------------------------------------ */ 
/* Flag variable and state											  */ 
/* ------------------------------------------------------------------ */ 
static unsigned char sensor_xcvu125_global_flags; 
unsigned char sensor_xcvu125_initialized[SENSOR_XCVU125_COUNT]; 
 
#define XCVU125_GLOBAL_INIT		(1 << 0)	/* initialize all sensors */ 
#define XCVU125_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */ 
 
/* ------------------------------------------------------------------ */ 
/* This section contains functions specific to the device.			  */ 
/* ------------------------------------------------------------------ */ 
unsigned char initialize_sensor_xcvu125(unsigned short i2c_addr){ 
	// YE: Nothing to initialize
    return 0x00; 
} 

unsigned char read_sensor_xcvu125(unsigned char *value, unsigned short i2c_addr){ 

    *value = 0x00; 

	// YE: Read: UltraScale Architecture System Monitor UG580 (v1.9.1) February 25, 2019
	// p.45 SYSMON Register Interface
	// p.48 Status Registers; Temperature 00h The 10 bit data is MSB justified in the 16-bit register
	// p.51 Figure 3-4: Measurement Registers
	// p.59 DRP registers are accessed from SYSMONE1 DRP interface, I2C, and JTAG TAP
	// p.62 DPR command Figure 3-10 and Table 3-11: CMD[3:0]=0001 (DRP read)
	// p.65 DRP I2C Interface - I2C slave allowing read/write access to the SYSMONE1 DRP interface.
	// p.73 I2C Read/Write Transfers Figure 3-18 and Table 3-19; DPR command p.62 Figure 3-10

	// Command is 4 bytes as DPR Read command is 32-bit (p.62 Figure 3-10)
	// [31:30] = xx (set to 00)
	// [29:26] = CMD [3:0]=0001 for DPR Read (p.63 Table 3-11)
	// [25:16] = DPR Address [9:0] = 0 for Temperature register
	// [15:0]  = DPR Data [15:0] - N/A for Read command, set to 0
    // 00_0001_0000000000_0000000000000000 > 0000_0100_0000_0000_0000_0000_0000_0000 > 0x04000000
    unsigned int  command = 0x04000000; // 4 bytes XCVU125 Command

	// Data Buffer 
	unsigned char data[2];	// 2 bytes Data Buffer as DPR Data is 16-bit

    // Check Sensor_I2C bus availability
    if (!monly_i2c_is_ready(i2c_addr)) {
    // print debug message   
    debug_printf("Sensor_I2C bus is not ready\n"); 
    return I2C_NOTREADY;
    }
	
    // Read Data
    // char i2c_dev_read_4bytesReg(unsigned short addr, unsigned int reg, unsigned char *pdata, unsigned char size) 
    unsigned char status;
    status = i2c_dev_read_4bytesReg(i2c_addr, command, data, 2);
    if (status != I2C_OK) {
    // print debug message
    debug_printf("XCVU125 temperature reading failed\n");
    return status;
    }

	// Temperature register [15:0], temperature data 10-bit -> Bit15=MSB Bit6=LSB 
	// 8 MSB from dataBuff[1]=Data[15:8]
	*value = data[1];

    return 0x00; //return non-zero value in case of reading error (sensor not updated)
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains Template sensor methods. 					  */ 
/* ------------------------------------------------------------------ */ 
 
/* Fill the Get Sensor Reading reply */ 
static char sensor_xcvu125_fill_rd(sensor_t *sensor, unsigned char *msg){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i, sval, error; 
     
    i = ((sensor_xcvu125_t *) sensor) - sensor_xcvu125; 
    if (sensor_xcvu125_initialized[i]) { 
        sensor_xcvu125_initialized[i] = initialize_sensor_xcvu125(sensor_xcvu125_ro[i].i2c_addr); 
    } 
    error = read_sensor_xcvu125(&sval, sensor_xcvu125_ro[i].i2c_addr); 
     
    /* Update sensor value */ 
    if (error == 0){ 
        sensor_threshold_update_s(&sensor_xcvu125[i].sensor, sval, 0); 
    } 

    return sensor_threshold_fill_reading(sensor, msg); 
 
} 
 
/* Sensor initialization. */ 
static char sensor_xcvu125_init(sensor_t *sensor){ 
 
    /* Get instance index using the pointer address */ 
    unsigned char i = ((sensor_xcvu125_t *) sensor) - sensor_xcvu125; 
     
    /* Execute init function */ 
    sensor_xcvu125_initialized[i] = initialize_sensor_xcvu125(sensor_xcvu125_ro[i].i2c_addr); 
     
    return 0;    
 
} 
 
/* ------------------------------------------------------------------ */ 
/* This section contains callbacks used to manage the sensor. 		  */ 
/* ------------------------------------------------------------------ */ 

/* YE: payload power off */
#ifdef NEED_CARRIER_CALLBACKS
CARRIER_UP_CALLBACK(sensor_xcvu125_carrier_up)
{
    /* schedule global XCVU125 init/update */
    sensor_xcvu125_global_flags = XCVU125_GLOBAL_INIT | XCVU125_GLOBAL_UPDATE;
}
CARRIER_DOWN_CALLBACK(sensor_xcvu125_carrier_down)
{
    /* unschedule global XCVU125 init/update */
    sensor_xcvu125_global_flags = 0;
}
#endif

/* 1 second callback */ 
TIMER_CALLBACK(1s, sensor_xcvu125_1s_callback){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_xcvu125_global_flags |= XCVU125_GLOBAL_UPDATE; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Initialization callback */ 
INIT_CALLBACK(sensor_xcvu125_init_all){ 
    unsigned char flags; 
 
    /* 
     * -> Save interrupt state and disable interrupts 
     *   Note: that ensure flags variable is not written by 
     *         two processes at the same time. 
     */ 
    save_flags_cli(flags); 
 
    /* Change flag to schedule and update */ 
    sensor_xcvu125_global_flags |= XCVU125_GLOBAL_INIT; 
 
    /* 
     * -> Restore interrupt state and enable interrupts 
     *   Note: restore the system 
     */ 
    restore_flags(flags); 
} 
 
/* Main loop callback */ 
MAIN_LOOP_CALLBACK(sensor_xcvu125_poll){ 
 
    unsigned char i, flags, gflags, pcheck, sval, error; 
 
    /* Disable interrupts */ 
    save_flags_cli(flags); 
 
    /* Saved flag state into a local variable */ 
    gflags = sensor_xcvu125_global_flags; 
 
    /* Clear flags */ 
    sensor_xcvu125_global_flags = 0; 
 
    /* Enable interrupts */ 
    restore_flags(flags); 
 
    if (gflags & XCVU125_GLOBAL_INIT) { 
 
        /* initialize all Template sensors */ 
        for (i = 0; i < SENSOR_XCVU125_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
            pcheck = sensor_xcvu125[i].sensor.s.status; 
 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                sensor_xcvu125_initialized[i] = initialize_sensor_xcvu125(sensor_xcvu125_ro[i].i2c_addr); 
            } else { 
                sensor_xcvu125_initialized[i] = 0xff;
            } 
		} 
    } 
 
    if (gflags & XCVU125_GLOBAL_UPDATE) { 
 
    	/* update all sensor readings */ 
        for (i = 0; i < SENSOR_XCVU125_COUNT; i++) { 
 
        	/* Check if the sensor is present         */ 
        	/*    e.g.: can be absent in case of RTM  */ 
        	pcheck = sensor_xcvu125[i].sensor.s.status; 
            if (!(pcheck & STATUS_NOT_PRESENT)) { 
                if (sensor_xcvu125_initialized[i]) { 
                    sensor_xcvu125_initialized[i] = initialize_sensor_xcvu125(sensor_xcvu125_ro[i].i2c_addr); 
                } 
                WDT_RESET; 
                error = read_sensor_xcvu125(&sval, sensor_xcvu125_ro[i].i2c_addr); 
                if (error == 0){ 
                    sensor_threshold_update_s(&sensor_xcvu125[i].sensor, sval, flags); 
                } 
            } 
        } 
    } 
 
} 
 
#endif /* CFG_SENSOR_XCVU125 */ 
