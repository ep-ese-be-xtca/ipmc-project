/* 
    Description:
    This header file defines the interfaces
    provided by the sensor_xcvu125.c module.

    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_XCVU125_H__
#define __MASTER_SENSOR_XCVU125_H__

#include <sensor.h>

/* Read-only info structure of an Template sensor */
typedef struct {
    sensor_ro_t s;
    unsigned short i2c_addr;
} sensor_xcvu125_ro_t;

/* Template sensor methods */
extern sensor_methods_t PROGMEM sensor_xcvu125_methods;

static char sensor_xcvu125_fill_rd(sensor_t *sensor, unsigned char *msg);

/* Auxiliary macro for defining Template sensor info */
#define SENSOR_XCVU125(s, i2c_addr_p, alert)		\
    {						\
        SA(sensor_xcvu125_methods, s, alert), \
        i2c_addr: (i2c_addr_p) \
    }
     
#endif /* __MASTER_SENSOR_XCVU125_H__ */
